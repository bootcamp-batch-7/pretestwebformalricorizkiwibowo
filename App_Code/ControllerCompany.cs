﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerCompany : ClassBase
{
    public ControllerCompany(ClassDatabasesDataContext _db) : base(_db)
    {

    }

    //GET DATA
    public Company[] Data()
    {
        return db.Companies.ToArray();
    }

    //Create
    public Company Create(string name, string address, string email, string telephone, int createdby, int flag)
    {
        Company company = new Company
        {
            UID = Guid.NewGuid(),
            Name = name,
            Address = address,
            Email = email,
            Telephone = telephone,
            CreatedBy = createdby,
            Flag = flag,
            CreatedAt = DateTime.Now
        };
        db.Companies.InsertOnSubmit(company);
        return company;
    }

    public Company Cari(string UID)
    {
        return db.Companies.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public Company Update(string UID, string name, string address, string email,string telephone, int flag)
    {
        var company = Cari(UID);

        if (company != null)
        {
            company.Name = name;
            company.Address = address;
            company.Email = email;
            company.Telephone = telephone;
            company.Flag = flag;
            return company;
        }
        else
            return null;
    }


    public Company Delete(string uid)
    {
        var company = Cari(uid);
        if (company != null)
        {
            var users = db.Users.Where(x => x.IDCompany == company.ID);
            db.Users.DeleteAllOnSubmit(users);
            db.Companies.DeleteOnSubmit(company);
            db.SubmitChanges();
        }
        return company;
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> company = new List<ListItem>();

        company.Add(new ListItem { Value = "0", Text = "-Pilih-" });
        company.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));
        return company.ToArray();
    }

}