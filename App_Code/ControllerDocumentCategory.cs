﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ControllerCompany
/// </summary>
public class ControllerDocumentCategory : ClassBase
{
    public ControllerDocumentCategory(ClassDatabasesDataContext _db) : base(_db)
    {

    }

    //GET DATA
    public DocumentCategory[] Data()
    {
        return db.DocumentCategories.ToArray();
    }

    //Create
    public DocumentCategory Create(string name,int createdby)
    {
        DocumentCategory documentCategory = new DocumentCategory
        {
            UID = Guid.NewGuid(),
            Name = name,
            CreatedBy = createdby,
            CreatedAt = DateTime.Now
        };
        db.DocumentCategories.InsertOnSubmit(documentCategory);
        return documentCategory;
    }

    public DocumentCategory Cari(string UID)
    {
        return db.DocumentCategories.FirstOrDefault(x => x.UID.ToString() == UID);
    }

    public DocumentCategory Update(string UID, string name)
    {
        var documentCategory = Cari(UID);

        if (documentCategory != null)
        {
            documentCategory.Name = name;
            return documentCategory;
        }
        else
            return null;
    }


    public DocumentCategory Delete(string uid)
    {
        var documentCategory = Cari(uid);
        //if (documentCategory != null)
        //{
        //    var users = db.Users.Where(x => x.IDPosition == documentCategory.ID);
        //    db.Users.DeleteAllOnSubmit(users);
        //    db.DocumentCategories.DeleteOnSubmit(documentCategory);
        //    db.SubmitChanges();
        //}
        return documentCategory;
    }

    public ListItem[] DropDownList()
    {
        List<ListItem> documentCategory = new List<ListItem>();

        documentCategory.Add(new ListItem { Value = "0", Text = "-Pilih-" });
        documentCategory.AddRange(Data().Select(x => new ListItem
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));
        return documentCategory.ToArray();
    }

}