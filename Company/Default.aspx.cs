﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                LoadData(db);
            }
        }
    }
    private void LoadData(ClassDatabasesDataContext db)
    {
        ControllerCompany controllerCompany = new ControllerCompany(db);
        repeaterCompany.DataSource = controllerCompany.Data();
        repeaterCompany.DataBind();
    }
    protected void repeaterCompany_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Company/Forms.aspx?uid=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var company = db.Companies.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());

                db.Companies.DeleteOnSubmit(company);
                db.SubmitChanges();
                LoadData(db);
            }
        }
    }
}