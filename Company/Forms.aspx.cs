﻿using ImageResizer.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Company_Default2 : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);
                var company = controllerCompany.Cari(Request.QueryString["uid"]);

                if (company != null)
                {
                    InputName.Text = company.Name;
                    InputAddress.Text = company.Address;
                    InputEmail.Text = company.Email;
                    InputTelephone.Text = company.Telephone;
                    InputFlag.Text = company.Flag.ToString();
                    InputCreatedBy.Text = company.CreatedBy.ToString();
                    ButtonOk.Text = "Update";
                    LabelTitle.Text = "Update Company";
                }
                else
                {
                    ButtonOk.Text = "Add Now";
                    LabelTitle.Text = "Add New Company";
                }
            }
        }
    }
    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);

                if (ButtonOk.Text == "Add Now")
                    controllerCompany.Create(
                        InputName.Text,
                        InputAddress.Text,
                        InputEmail.Text,
                        InputTelephone.Text,
                        int.Parse(InputCreatedBy.Text),
                        int.Parse(InputFlag.Text)
                        );
                else if (ButtonOk.Text == "Update")
                    controllerCompany.Update(
                        Request.QueryString["UID"],
                        InputName.Text,
                        InputAddress.Text,
                        InputEmail.Text,
                        InputTelephone.Text,
                        int.Parse(InputFlag.Text));
                db.SubmitChanges();
                Response.Redirect("/Default.aspx");
            }
        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/Default.aspx");
    }
}