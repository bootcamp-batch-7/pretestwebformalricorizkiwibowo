USE [master]
GO
/****** Object:  Database [PretestAgs]    Script Date: 8/4/2023 10:02:26 AM ******/
CREATE DATABASE [PretestAgs] ON  PRIMARY 
( NAME = N'PretestAgs', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PretestAgs.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PretestAgs_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\PretestAgs_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PretestAgs].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PretestAgs] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PretestAgs] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PretestAgs] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PretestAgs] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PretestAgs] SET ARITHABORT OFF 
GO
ALTER DATABASE [PretestAgs] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PretestAgs] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PretestAgs] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PretestAgs] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PretestAgs] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PretestAgs] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PretestAgs] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PretestAgs] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PretestAgs] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PretestAgs] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PretestAgs] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PretestAgs] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PretestAgs] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PretestAgs] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PretestAgs] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PretestAgs] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PretestAgs] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PretestAgs] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PretestAgs] SET  MULTI_USER 
GO
ALTER DATABASE [PretestAgs] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PretestAgs] SET DB_CHAINING OFF 
GO
USE [PretestAgs]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 8/4/2023 10:02:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK__Company__3214EC27FE111109] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Document]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Document](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDCategory] [int] NULL,
	[Name] [varchar](255) NULL,
	[Description] [text] NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK__Document__3214EC272B29060B] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DocumentCategory]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DocumentCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK__Document__3214EC278D3D1B79] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Position]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Position](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK__Position__3214EC27DFEAA64E] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDPosition] [int] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK__User__3214EC27CC51523F] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Company] ON 

INSERT [dbo].[Company] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (2, N'1d51463c-1fc1-4ad1-a07e-f5bfe82b3181', N'BAGUS', N'sss', N'sss', N'sss', 1, 0, CAST(N'2023-08-03T10:39:42.750' AS DateTime))
INSERT [dbo].[Company] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (3, N'da81cf76-a07a-40fb-b9e7-d977edd766b7', N'string', N'string', N'string', N'string', 0, 0, CAST(N'2023-08-03T16:19:10.283' AS DateTime))
INSERT [dbo].[Company] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (4, N'4f2d8a38-56ab-4113-ba73-445b3f25a760', N'123', N'123', N'123', N'123', 0, 123, CAST(N'2023-08-03T20:20:16.347' AS DateTime))
INSERT [dbo].[Company] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (5, N'b492bd3c-37ce-41a8-9171-0e6461aeb58b', N'kompaniname', N'kompani', N'kompaniemail', N'telepon', 1, 1, CAST(N'2023-08-04T09:00:19.190' AS DateTime))
SET IDENTITY_INSERT [dbo].[Company] OFF
GO
SET IDENTITY_INSERT [dbo].[Document] ON 

INSERT [dbo].[Document] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (2, N'27fefc34-fe89-4a70-88d1-d6cbb801679a', 2, 1, N'asd', N'asd', 1, 1, CAST(N'2023-08-03T11:28:07.570' AS DateTime))
INSERT [dbo].[Document] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (3, N'e714653a-d869-4354-b538-2b6f1b835731', 2, 1, N'asd', N'asd', 0, 0, CAST(N'2023-08-03T11:31:44.307' AS DateTime))
INSERT [dbo].[Document] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (5, N'b9646b69-1e8f-4840-9b1f-dcdbf99b1895', 2, 1, N'string', N'string', 0, 0, CAST(N'2023-08-03T16:09:13.183' AS DateTime))
INSERT [dbo].[Document] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (6, N'b859a42b-89cf-4620-9173-42b94bcaf6d1', 2, 1, N'name', N'OKE MANTAP', 0, 1, CAST(N'2023-08-04T09:49:03.910' AS DateTime))
SET IDENTITY_INSERT [dbo].[Document] OFF
GO
SET IDENTITY_INSERT [dbo].[DocumentCategory] ON 

INSERT [dbo].[DocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (1, N'2fa05373-c225-431e-b56a-cd6d120ae13a', N'docsupdate', 0, CAST(N'2023-08-03T11:07:53.710' AS DateTime))
INSERT [dbo].[DocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (2, N'b0878d3f-32e7-4a7a-b7d0-0584478a1925', N'123123', 123123, CAST(N'2023-08-03T21:06:32.180' AS DateTime))
SET IDENTITY_INSERT [dbo].[DocumentCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Position] ON 

INSERT [dbo].[Position] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (1, N'772d4fe8-16b1-47a1-a2ef-919826901b36', N'asdasdas', 0, CAST(N'2023-08-03T10:49:10.650' AS DateTime))
INSERT [dbo].[Position] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (2, N'48659078-a274-4b2f-9425-cebdcde74903', N'Admin', 2, CAST(N'2023-08-03T20:36:08.490' AS DateTime))
INSERT [dbo].[Position] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (3, N'f6dd3b58-5cb1-405f-970e-9c50b9346889', N'123', 123, CAST(N'2023-08-03T20:37:06.467' AS DateTime))
INSERT [dbo].[Position] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (4, N'6379881c-a9d9-4b3d-99f1-6aa3373e1f59', N'123', 123, CAST(N'2023-08-03T20:43:30.627' AS DateTime))
SET IDENTITY_INSERT [dbo].[Position] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (26, N'899bde41-5869-4f5a-a743-b69d7718392e', 2, 2, N'nama123', N'addrress', N'email@', N'tlp123', N'alrico', N'', N'role', 1, 1, CAST(N'2023-08-04T09:27:02.493' AS DateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_Company] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_Company]
GO
ALTER TABLE [dbo].[Document]  WITH CHECK ADD  CONSTRAINT [FK_Document_DocumentCategory] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[DocumentCategory] ([ID])
GO
ALTER TABLE [dbo].[Document] CHECK CONSTRAINT [FK_Document_DocumentCategory]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Company] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Company]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Position] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[Position] ([ID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Position]
GO
/****** Object:  StoredProcedure [dbo].[sp_createCompany]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createCompany]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Company]
	   ([UID],
	[Name],
	[Address],
	[Email],
	[Telephone],
	[Flag],
	[CreatedBy],
	[CreatedAt])
	VALUES
		(NEWID(),
		@name,
		@address,
		@email,
		@telephone,
		@flag,
		@createdby,
		GETDATE()
		)
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createDocument]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createDocument]
	-- Add the parameters for the stored procedure here
	@idcompany int,
	@idcategory int,
	@name varchar(50),
	@description text,
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Document]
	   ([UID],
		[IDCompany],
		[IDCategory],
		[Name],
		[Description],
		[Flag],
		[CreatedBy],
		[CreatedAt])
	VALUES
		(NEWID(),
		@idcompany,
		@idcategory, 
		@name,
		@description, 
		@flag, 
		@createdby,
		GETDATE()
		)
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createDocumentCategory]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createDocumentCategory]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[DocumentCategory]
	   ([UID],
	[Name],
	[CreatedBy],
	[CreatedAt])
	VALUES
		(NEWID(),
		@name,
		@createdby,
		GETDATE()
		)
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createPosition]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createPosition]
	-- Add the parameters for the stored procedure here
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Position]
	   ([UID],
	[Name],
	[CreatedBy],
	[CreatedAt])
	VALUES
		(NEWID(),
		@name,
		@createdby,
		GETDATE()
		)
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_createUser]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_createUser]
	-- Add the parameters for the stored procedure here
	@idcompany int,
	@idposition int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[User]
	   ([UID],
		[IDCompany],
		[IDPosition],
		[Name],
		[Address],
		[Email],
		[Telephone],
		[Username],
		[Password],
		[Role],
		[Flag],
		[CreatedBy],
		[CreatedAt])
	VALUES
		(NEWID(),
		@idcompany,
		@idposition,
		@name,
		@address,
		@email,
		@telephone,
		@username,
		CONVERT(VARCHAR(255), HashBytes('MD5', @password), 2),
		@role,
		@flag, 
		@createdby,
		GETDATE()
		)
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteCompany]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE
	FROM [dbo].[Company] 
	WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteDocument]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE
	FROM [dbo].[Document] 
	WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deletePosition]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deletePosition]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE
	FROM [dbo].[Position] 
	WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_deleteUser]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_deleteUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE
	FROM [dbo].[User] 
	WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_loginUser]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_loginUser]
	@email varchar(50),
	@password varchar(255),
	@retVal int OUTPUT
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		[ID],[Username],[Email],[Role],[CreatedAt]
	FROM [User]
	WHERE Email = @email and [Password] = CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2)
	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateCompany]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateCompany]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(15),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[Company] SET
	[Name]= @name,
	[Address]=@address,
	[Email]=@email,
	[Telephone]=@telephone,
	[Flag]=@flag,
	[CreatedBy]=@createdby
		WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateDocument]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateDocument]
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
	@idcategory int,
	@name varchar(50),
	@description text,
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[Document] SET
	[IDCompany] = @idcompany,
	[IDCategory] = @idcategory,
	[Name]= @name,
	[Description] =@description,
	[Flag]=@flag,
	[CreatedBy]=@createdby
		WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateDocumentCategory]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateDocumentCategory]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[DocumentCategory] SET
	[Name]= @name,
	[CreatedBy]=@createdby
		WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updatePosition]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updatePosition]
	-- Add the parameters for the stored procedure here
	@id int,
	@name varchar(50),
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[Position] SET
	[Name]= @name,
	[CreatedBy]=@createdby
		WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_updateUser]    Script Date: 8/4/2023 10:02:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_updateUser]
	-- Add the parameters for the stored procedure here
	@id int,
	@idcompany int,
	@idposition int,
	@name varchar(50),
	@address text,
	@email varchar(50),
	@telephone varchar(14),
	@username varchar(50),
	@password varchar(255),
	@role varchar(50),
	@flag int,
	@createdby int,
	@retVal int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE [dbo].[User] SET
	[IDCompany] = @idcompany,
	[IDPosition] = @idposition,
	[Name]= @name,
	[Address]=@address,
	[Email]=@email,
	[Username] = @username,
	[Password] =  CONVERT(VARCHAR(32), HashBytes('MD5', @password), 2),
	[Telephone]=@telephone,
	[Flag]=@flag,
	[CreatedBy]=@createdby
		WHERE ID = @id

	IF (@@ROWCOUNT > 0)
	BEGIN
		SET @retVal = 200
	END
	ELSE
	BEGIN
		SET @retVal = 500
	END
END
GO
USE [master]
GO
ALTER DATABASE [PretestAgs] SET  READ_WRITE 
GO
