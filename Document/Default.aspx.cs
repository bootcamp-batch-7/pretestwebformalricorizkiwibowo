﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                LoadData(db);
            }
        }
    }
    private void LoadData(ClassDatabasesDataContext db)
    {
        ControllerDocument controllerDOcument = new ControllerDocument(db);
        repeaterDocument.DataSource = db.Documents
        .Select(x => new
        {
            x.ID,
            x.IDCompany,
            NameCompany = x.Company.Name,
            x.IDCategory,
            NameCategory = x.DocumentCategory.Name,
            x.Name,
            x.Description,
            x.Flag,
            x.CreatedBy

        }).ToArray();
        repeaterDocument.DataBind();
    }
    protected void repeaterDocument_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Document/Forms.aspx?ID=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var document = db.Documents.FirstOrDefault(x => x.ID.ToString() == e.CommandArgument.ToString());
                db.Documents.DeleteOnSubmit(document);
                db.SubmitChanges();
                LoadData(db);
            }
        }
    }
}