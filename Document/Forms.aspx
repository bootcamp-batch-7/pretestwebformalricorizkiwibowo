﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Forms.aspx.cs" Inherits="Document_Forms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="Server">
    <asp.label id="LabelTitle" runat="server"></asp.label>
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div class="row">
        <div class="col-md-12">

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label>Company</label>
                        <asp:DropDownList ID="ListCompany" CssClass="form-control" runat="server" Width="100%"></asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <label>Category</label>
                        <asp:DropDownList ID="ListCategory" CssClass="form-control" runat="server" Width="100%"></asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <label>Name</label>
                        <asp:TextBox ID="InputName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <label>Flag</label>
                        <asp:DropDownList ID="InputFlag" CssClass="form-control" runat="server">
                            <asp:ListItem Text="Aktif" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Tidak Aktif" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-6">
                        <label>Created By</label>
                        <asp:TextBox ID="InputCreatedBy" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>Description</label>
        <asp:TextBox ID="InputDescription" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
    </div>
    <div class="mt-1">
        <asp:Button ID="btnOk" CssClass="btn btn-success btn-sm" runat="server" Text="Add Now" OnClick="btnOk_Click" />
        <a href="Default.aspx" class="btn btn-danger btn-sm">Cancel</a>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" runat="Server">
</asp:Content>

