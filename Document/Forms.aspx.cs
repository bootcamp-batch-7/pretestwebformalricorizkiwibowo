﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.PeerToPeer;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Document_Forms : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);
                ControllerDocumentCategory controllerDocumentCategory = new ControllerDocumentCategory(db);
                ListCompany.Items.AddRange(controllerCompany.DropDownList());
                ListCategory.Items.AddRange(controllerDocumentCategory.DropDownList());
                if (Request.QueryString["ID"] != null)
                {
                    ControllerDocument controllerDocument = new ControllerDocument(db);
                    var document = controllerDocument.Cari(int.Parse(Request.QueryString["ID"]));
                    if (document != null)
                    {
                        ListCompany.SelectedValue = document.IDCompany.ToString();
                        ListCategory.SelectedValue = document.IDCategory.ToString();
                        InputName.Text = document.Name;
                        InputDescription.Text = document.Description;
                        InputFlag.Text = document.Flag.ToString();
                        InputCreatedBy.Text = document.CreatedBy.ToString();
                        btnOk.Text = "Update";
                        LabelTitle.InnerText = "Update User";
                    }
                }
                else
                {
                    btnOk.Text = "Add New";
                    LabelTitle.InnerText = "Add New User";
                }
            }
            }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                ControllerDocument controllerDocument = new ControllerDocument(db);
                if (btnOk.Text == "Add New")
                {
                    controllerDocument.Create(
                        int.Parse(ListCompany.SelectedValue),
                        int.Parse(ListCategory.SelectedValue),
                        InputName.Text,
                        InputDescription.Text,
                        int.Parse(InputFlag.Text),
                        int.Parse(InputCreatedBy.Text)
                        );
                }
                else if (btnOk.Text == "Update")
                {
                    controllerDocument.Update(
                        int.Parse(Request.QueryString["ID"]),
                        int.Parse(ListCompany.SelectedValue),
                        int.Parse(ListCategory.SelectedValue),
                        InputName.Text,
                        InputDescription.Text,
                        int.Parse(InputFlag.Text),
                        int.Parse(InputCreatedBy.Text)
                        );
                }
                db.SubmitChanges();
                Response.Redirect("/Document/Default.aspx");
            }
        }
    }
    }
