﻿using ImageResizer.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Update : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                ControllerCompany controllerCompany = new ControllerCompany(db);
                ControllerPosition controllerPosition = new ControllerPosition(db);
                ListCompany.Items.AddRange(controllerCompany.DropDownList());
                ListPosition.Items.AddRange(controllerPosition.DropDownList());
                ControllerUser controllerUser = new ControllerUser(db);
                var user = controllerUser.Cari(int.Parse(Request.QueryString["ID"]));
                if (user != null)
                {
                    ListCompany.SelectedValue = user.IDCompany.ToString();
                    ListPosition.SelectedValue = user.IDPosition.ToString();
                    InputName.Text = user.Name;
                    InputAddress.Text = user.Address;
                    InputEmail.Text = user.Email;
                    InputRole.Text = user.Role;
                    InputEmail.Text = user.Email;
                    InputUser.Text = user.Username;
                    InputCreatedBy.Text = user.CreatedBy.ToString();
                    InputFlag.Text = user.Flag.ToString();
                    InputTelephone.Text = user.Telephone;
                    btnOk.Text = "Update";
                    LabelTitle.InnerText = "Update User";
                }
                else
                {
                    btnOk.Text = "Add Now";
                    LabelTitle.InnerText = "Add Now User";
                }
        }
        }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                ControllerUser controllerUser = new ControllerUser(db);
                if (btnOk.Text == "Add New")
                {
                    controllerUser.Create(
                        int.Parse(ListCompany.SelectedValue),
                        int.Parse(ListPosition.SelectedValue),
                        InputName.Text,
                        InputAddress.Text,
                        InputEmail.Text,
                        InputUser.Text,
                        InputPassword.Text,
                        InputRole.Text,
                        InputTelephone.Text,
                        int.Parse(InputCreatedBy.Text),
                        int.Parse(InputFlag.Text)
                        );
                }
                else if(btnOk.Text == "Update")
                {
                    controllerUser.Update(
                        int.Parse(Request.QueryString["ID"]),
                        int.Parse(ListCompany.SelectedValue),
                        int.Parse(ListPosition.SelectedValue),
                        InputName.Text,
                        InputAddress.Text,
                        InputEmail.Text,
                        InputUser.Text,
                        InputPassword.Text,
                        InputRole.Text,
                        InputTelephone.Text,
                        int.Parse(InputCreatedBy.Text),
                        int.Parse(InputFlag.Text));
                }
                db.SubmitChanges();
                Response.Redirect("/User/Default.aspx");
            }
        }
    }
}