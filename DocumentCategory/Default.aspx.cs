﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCategory_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                LoadData(db);
            }
        }
    }
    private void LoadData(ClassDatabasesDataContext db)
    {
        ControllerDocumentCategory controllerDocumentCategory = new ControllerDocumentCategory(db);
        repeaterDocumentCategory.DataSource = controllerDocumentCategory.Data();
        repeaterDocumentCategory.DataBind();
    }
    protected void repeaterDocumentCategory_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/DocumentCategory/Forms.aspx?uid=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var documentCatergory = db.DocumentCategories.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());
                db.DocumentCategories.DeleteOnSubmit(documentCatergory);
                db.SubmitChanges();
                LoadData(db);
            }
        }
    }
}