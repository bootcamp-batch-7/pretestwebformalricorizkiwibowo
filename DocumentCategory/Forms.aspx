﻿<%@ Page Title="" Language="C#" MasterPageFile="~/assets/MasterPage.master" AutoEventWireup="true" CodeFile="Forms.aspx.cs" Inherits="DocumentCategory_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderTitle" runat="Server">
    <asp:Label ID="LabelTitle" runat="server"></asp:Label>
    <hr />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderBody" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-body">
                    <asp:Label ID="LiteralWarning" runat="server"></asp:Label>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-label bold">Name</label>
                                <asp:TextBox ID="InputName" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                                 <div class="col-md-6">
                                <label class="form-label bold">Created By</label>
                                <asp:TextBox ID="InputCreatedBy" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                        <br />
                        <asp:Button ID="ButtonOk" CssClass="btn btn-success ctn-sm" runat="server" Text="Add Now" OnClick="ButtonOk_Click" />
                        <asp:Button ID="ButtonKeluar" CssClass="btn btn-danger ctn-sm" runat="server" Text="Cancel" OnClick="ButtonKeluar_Click" />
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolderJavascript" runat="Server">
</asp:Content>

