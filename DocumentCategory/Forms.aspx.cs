﻿using ImageResizer.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DocumentCategory_Default2 : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                ControllerDocumentCategory controllerDocumentCategory = new ControllerDocumentCategory(db);
                var documentCategory = controllerDocumentCategory.Cari(Request.QueryString["UID"]);

                if (documentCategory != null)
                {
                    InputName.Text = documentCategory.Name;
                    InputCreatedBy.Text = documentCategory.CreatedBy.ToString();
                    ButtonOk.Text = "Update";
                    LabelTitle.Text = "Update documentCategory ";
                }
                else
                {
                    ButtonOk.Text = "Add Now";
                    LabelTitle.Text = "Add New documentCategory ";
                }
            }
        }
    }
    protected void ButtonOk_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                ControllerDocumentCategory controllerDocumentCategory = new ControllerDocumentCategory(db);

                if (ButtonOk.Text == "Add Now")
                    controllerDocumentCategory.Create(
                        InputName.Text,
                        int.Parse(InputCreatedBy.Text));
                else if (ButtonOk.Text == "Update")
                    controllerDocumentCategory.Update(
                        Request.QueryString["UID"],
                        InputName.Text);
                db.SubmitChanges();
                Response.Redirect("/DocumentCategory/Default.aspx");
            }
        }
    }

    protected void ButtonKeluar_Click(object sender, EventArgs e)
    {
        Response.Redirect("/DocumentCategory/Default.aspx");
    }
}