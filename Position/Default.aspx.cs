﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Position_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                LoadData(db);
            }
        }
    }
    private void LoadData(ClassDatabasesDataContext db)
    {
        ControllerPosition controllerPosition = new ControllerPosition(db);
        repeaterPosition.DataSource = controllerPosition.Data();
        repeaterPosition.DataBind();
    }
    protected void repeaterPosition_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/Position/Forms.aspx?uid=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var position = db.Positions.FirstOrDefault(x => x.UID.ToString() == e.CommandArgument.ToString());
                db.Positions.DeleteOnSubmit(position);
                db.SubmitChanges();
                LoadData(db);
            }
        }
    }
}