﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class User_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
            {
                LoadData(db);
            }
        }
    }
    private void LoadData(ClassDatabasesDataContext db)
    {
        ControllerUser controllerUser = new ControllerUser(db);
        repeaterUser.DataSource = db.Users
        .Select(x => new
        {
            x.ID,
            x.IDCompany,
            NameCompany = x.Company.Name,
            x.IDPosition,
            NamePosition = x.Position.Name,
            x.Name,
            x.Address,
            x.Email,
            x.Telephone,
            x.Username,
            x.Role,
            x.Flag,
            x.CreatedBy

        }).ToArray();
        repeaterUser.DataBind();
    }
    protected void repeaterUser_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        using (ClassDatabasesDataContext db = new ClassDatabasesDataContext())
        {
            if (e.CommandName == "Update")
            {
                Response.Redirect("/User/Forms.aspx?ID=" + e.CommandArgument.ToString());
            }
            else if (e.CommandName == "Delete")
            {
                var user = db.Users.FirstOrDefault(x => x.ID.ToString() == e.CommandArgument.ToString());
                db.Users.DeleteOnSubmit(user);
                db.SubmitChanges();
                LoadData(db);
            }
        }
    }
}